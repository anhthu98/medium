// window.onload = function () {
//   initAppItem();
// };
window.onscroll = function() {
	metabarFixed();
	sidebarFixed();
};

var navFixed = document.getElementById("nav--bottom");
var sticky = navFixed.offsetTop;

var sbFixed = document.getElementById("wrapper-sidebar");
var sticky_sidebar = sbFixed.offsetTop;

function metabarFixed() {
  if (window.pageYOffset > sticky) {
    navFixed.classList.add("sticky");
  } else {
    navFixed.classList.remove("sticky");
  }
}

function sidebarFixed() {
  if (window.pageYOffset > sticky_sidebar) {
    sbFixed.classList.add("sticky_sidebar");
  } else {
    sbFixed.classList.remove("sticky_sidebar");
  }
}


// slick navigation
        
$(window).resize(function(){
    var width = $(window).width();
    console.log(width);
    if (width <= 768){
      $('.nav-list').slick({
        speed: 200,
        slidesToShow: 3,
        slidesToScroll:3 ,
        arrows: true,
        variableWidth: true,
        infinite: false
      })
    }
    else if(width > 768){
      $('.nav-list').removeSlick('.nav-list');
    }
});
  



